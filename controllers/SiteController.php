<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Notas;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\helpers\ArrayHelper;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    
     public function actionTodasnotas(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Notas::find(),
             'pagination'=>[
                 'pageSize'=>0,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['usuario','fecha','hora','Mensaje'],
            "titulo"=>"Todas las Notas",
            "enunciado"=>"",
        ]);
    
    }
    
     public function actionNotasfecha(){
         
      
      
        
        $dataNotasfechas = new SqlDataProvider([
            'sql' => "SELECT mensaje,hora FROM notas where fecha = '$dataFechas'"
           ]);   
        
         return $this->render("resultado",[
            "resultadofechas"=>$dataNotasfechas,
           
            "campos"=>['mensaje','hora'],
        ]);
        
    }
    
    
    public function actionListar(){
        $dataProvider = new ActiveDataProvider([
             'query' => Notas::getFechas(),
             'pagination'=>[
                 'pageSize'=>10,
             ]
         ]);

        
        return $this->render("listar",[
            "resultados"=>$dataProvider,
        ]);
    }
    
    protected function juntar($registro){
        $a="";
        foreach ($registro->getMensajes() as $r) {
            $a.="<div class='bg-info'>$r->Mensaje</div>";
            $a.="<div class='bg-info'>$r->hora</div>";
        }
        return $a;
        
    }
    
    public function actionListado(){
        
        $fechas=Notas::getFechas()->all();
        foreach ($fechas as $indice => $registro) {
            $items[$indice]['header']=$registro->fecha;
            //$items[$indice]['content']= implode("<br>",ArrayHelper::getColumn($registro->getMensajes(),"Mensaje"));
            $items[$indice]['content']=$this->juntar($registro);
        }
        
        return $this->render("__listar",[
            "items"=>$items,
        ]);
        
        
    }

}
