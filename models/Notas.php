<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notas".
 *
 * @property int $id
 * @property int $usuario
 * @property string $fecha
 * @property string $hora
 * @property string $Mensaje
 *
 * @property Usuarios $usuario0
 */
class Notas extends \yii\db\ActiveRecord
{
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario'], 'integer'],
            [['fecha', 'hora'], 'safe'],
            [['Mensaje'], 'string'],
            [['usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['usuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario' => 'Usuario',
            'fecha' => 'Fecha',
            'hora' => 'Hora',
            'Mensaje' => 'Mensaje',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario0()
    {
        return $this->hasOne(Usuarios::className(), ['id' => 'usuario']);
    }
    
    public static function getFechas(){
        return self::find()->select("fecha")->distinct();
        
    }
    
       
    
    public static function getNotasdeFecha($fecha){
        return self::find()
                ->select("Mensaje,hora")
                ->where("fecha = '$fecha'")
                ->all();
    }
    
    
    /**
     * Este metodo devuelve todas las notas de mi fecha
     * @return type model Notas
     */
    public function getMensajes(){
        return $this->find()->where("fecha='$this->fecha'")->all();
    }

}
