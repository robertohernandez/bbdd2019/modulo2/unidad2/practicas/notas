<?php

namespace app\models;
use yii\captcha\Captcha;
use yii\captcha\CaptchaAction;
use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $id
 * @property string $usuario
 * @property string $password
 * @property string $email
 * @property string $authKey
 * @property string $accessToken
 * @property int $activate
 *
 * @property Notas[] $notas
 */
class Usuarios extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $password_repeat;
    public $codigo;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
   public function rules() {
        return [
            [['usuario', 'password'], 'string', 'max' => 255],
            [['usuario', 'password', 'password_repeat'], 'required', 'message' => 'El campo {attribute} es obligatorio'],
            // que el usuario no exista
            ['usuario', 'unique', 'message' => 'El usuario ya existe en el sistema'], //unique ya consulta en la bd si existe el usuario y lo valida o no
            ['password', 'string', 'min' => 4, 'tooShort' => 'la contraseña debe tener al menos 6 caracteres'],//cambiar message por tooshort
            //comparacion de contraseñas
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'operator' => '==', 'message' => 'Las contraseñas deben coincidir'], //compara el password con el password_repeat
            // validacion de captcha
            //['codigo', 'captcha', 'message' => 'No coincide el codigo mostrado'], // esto funcionaria correctamente sin AJAX
            //Con AJAX existe un bug lo corregimos con una funcion 
            ['codigo', 'codeVerify'],//se utiliza codeVerify con una funcion creada mas abajo en lugar del captcha pq algunas veces en ajax da problemas al enviar
            ['codigo','required','message'=>'Debes escribir algo en los codigos'],
            
            //colocar los campos que necesito que pase en la asignacion masiva
            [['password_repeat', 'password', 'usuario', 'codigo'], 'safe'],//campos seguros safe para asignacion masiva . Los required introducidos por rules ya se asignan como safe.
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario' => 'Usuario',
            'password' => 'Password',
            'password_repeat' => 'Repite la contraseña',
            'codigo' => 'Escribe los codigos que ves'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotas()
    {
        return $this->hasMany(Notas::className(), ['usuario' => 'id']);
    }
    
      public function codeVerify($attribute) {
        /* nombre de la accion del controlador */
        $captcha_validate = new  \yii\captcha\CaptchaAction('captcha', Yii::$app->controller);
        
        
        if ($this->$attribute) {
            $code = $captcha_validate->getVerifyCode();
            if ($this->$attribute != $code) {
                $this->addError($attribute, 'Ese codigo de verificacion no es correcto');
            }
        }
        
    }

    public static function findIdentity($id) {
        return static::findOne(['id' => $id]);
    }

    public static function findByUsername($username) {
        return static::findOne(['usuario' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /* public function validatePassword($password)
      {
      return Yii::$app->security->validatePassword($password, $this->password_hash);
      } */

    public function validatePassword($password) {
        return $this->password === $password;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return null;
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        return null;
    }
}
