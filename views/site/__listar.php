<?php
use yii\jui\Accordion;
use app\widgets\Hola;

 echo Accordion::widget([
    'items' => $items,
    'options' => ['tag' => 'div'],
    'itemOptions' => ['tag' => 'div'],
    'headerOptions' => ['tag' => 'h3'],
    'clientOptions' => ['collapsible' => false],
]);
 
 echo Hola::widget(['message'=>'Estoy alucinando']);